# case

供接案用的簡單上版工具

## 資料夾結構
./group_vars Ansible 運行所需相關設定

./keys 供存放不同環境主機的私鑰

./deploy.yml 上版主程式

./development 開發環境主機設定

./test 測試環境主機設定

./production 線上環境主機設定

## 使用說明
1. 將keys資料夾裡面的檔案，針對development.pem、test.pem、production.pem環境的不同，換上連入該環境所需的私鑰
2. 將development、test、production裡面的設定，修改成符合你主機的環境
3. 將group_vars資料夾中的git.yml、line.yml、os.yml裡面的設定，修改成符合你主機的環境
4. 運行Ansible-Playbook

上版

```
ansible-playbook -i [你的主機設定，好比development、test、production] deploy.yml
```

退回上一版

```
ansible-playbook -i [你的主機設定，好比development、test、production] restore.yml
```

## 關於Line Notify Token
針對如何取得Line Notify Token，請參考以下別人寫的文章

[https://www.oxxostudio.tw/articles/201806/line-notify.html](https://www.oxxostudio.tw/articles/201806/line-notify.html)